const Message = require('./message.model.js');

module.exports = {
	saveMessage(message) {
		Message.create(message)
			.catch(err => {
				console.error(err);
			});
	},

	async getMessages (req, res) {
		try {
			const messages = await Message
				.find()
				.select('-_id')
				.exec();

			res.status(200).json({ data: { messages } });
		} catch (err) {
			console.error(err);
			return res.status(400).send({ message: err.message });
		}
	}
}

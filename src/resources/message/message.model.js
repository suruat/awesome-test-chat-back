const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({
	text: {
		type: String,
		trim: true
	},
	type: String,
	author: {
		type: String,
		required: true
	},
	timestamp: {
		type: Date,
		required: true
	}
});

module.exports = mongoose.model('Message', messageSchema);

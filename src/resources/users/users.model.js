const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true,
		unique: true,
		trim: true
	},
	password: {
		type: String,
		required: true
	},
	username: String
});

async function passwordHashing () {
	if (!this.isModified('password')) {
		return;
	}

	try{
		const hash = await bcrypt.hash(this.password, 12);
		this.password = hash;
	} catch (err) {
		console.error(err);
		throw err;
	}
}

async function setUsername () {
	const username = this.email.match(/(.+)(?=@)/g)[0];
	console.log('setUsername', this);

	this.username = username;
}

UserSchema.pre('save', async function() {
	await passwordHashing.call(this);
	await setUsername.call(this);
});

UserSchema.methods.checkPassword = function (password) {
	const passwordHash = this.password
	return bcrypt.compare(password, passwordHash);
}

module.exports = mongoose.model('User', UserSchema);

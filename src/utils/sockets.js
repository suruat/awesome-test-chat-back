const socketJWT = require('socketio-jwt');
const config = require('../configs/dev.js');
const messagesController = require('../resources/message/message.controller.js');

module.exports = function (io) {
	io.on('connect', socketJWT.authorize({
		secret: config.jwt.secret,
		timeout: 15000,
		callback: false
	}))
	.on('authenticated', socket => {
		// User join the chat
		socket.on('join', username => {
			const message = {
				type: 'join',
				author: username,
				timestamp: Date.now()
			};
			messagesController.saveMessage(message);
			io.emit('join', message);
		});

		// User send message
		socket.on('send-message', incoming => {
			const message = {
				text: incoming.text,
				author: incoming.author,
				timestamp: Date.now()
			};
			messagesController.saveMessage(message);
			io.emit('messagePosted', message);
		});

		// User leaving the chat
		socket.on('leave', username => {
			const message = {
				type: 'leave',
				author: username,
				timestamp: Date.now()
			};
			messagesController.saveMessage(message);
			io.emit('leave', message);
		});
	});

}

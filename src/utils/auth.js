const jwt = require('jsonwebtoken');

const config = require('../configs/dev.js');
const User = require('../resources/users/users.model.js');

module.exports.protect = async (req, res, next) => {
	try {
		if (
			!req.headers.authorization ||
			!req.headers.authorization.includes('Bearer')
		) {
			throw new Error('Not Authorized')
		}

		const payload = await jwt.verify(
			req.headers.authorization.split('Bearer ')[1],
			config.jwt.secret
		);

		if (!payload) {
			throw new Error('Token Expired')
		}

		const user = await User.findById(payload.id)
			.select('-password');

		req.user = user;
		next();
	} catch (err) {
		console.error(err)

		return res.status(401).send({message: err.message})
	}
}

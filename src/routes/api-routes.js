const Router = require('express').Router;
const messagesController = require('../resources/message/message.controller.js');

const router = Router();

router.get('/messages', messagesController.getMessages);

module.exports = router;

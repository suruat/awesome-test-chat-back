const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const helmet = require('helmet');
const socketServer = require('socket.io');

const config = require('./configs/dev.js');
const connectDB = require('./utils/db.js');
const protect = require('./utils/auth.js').protect;
const subscribeToSocketEvents =  require('./utils/sockets.js');

// Routes Imports
const authRoutes = require('./routes/auth-routes.js');
const apiRoutes = require('./routes/api-routes.js');

const app = express();

app.disable('x-powered-by');

// Middlewares
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

function setCORSHeaders (req, res, next) {
	res.header('Access-Control-Allow-Origin', config.allowedOrigins.join(', '));
	res.header('Access-Control-Allow-Headers', 'Content-Type');
	next();
}
// Routes
app.use('/auth', setCORSHeaders, authRoutes);
app.use('/api', cors(), protect, apiRoutes);

(async _ => {
	try{
		await connectDB();

		const server = app.listen(config.port, _ => {
			console.log(`Server is running on port ${config.port}`);
			console.log(`API endpoints are available on http://localhost:${config.port}/api`);
			console.log(`Auth endpoints are available on http://localhost:${config.port}/auth`);
		});

		const io = socketServer(server, {
			origins: config.allowedOrigins
		});

		subscribeToSocketEvents(io);

	} catch (err) {
		console.error(err);
	}
})();

module.exports = {
	port: process.env.PORT || 8888,
	// dbUrl: 'mongodb://localhost:27017/awesome-chat',
	// dbUrl: 'mongodb://mongo:27017/awesome-chat',
	dbUrl: 'mongodb+srv://taurus90:<password>@cluster0-32len.mongodb.net/test?retryWrites=true&w=majority',
	jwt: {
		secret: 'awesome-chat-secret',
		expires: '3h'
	},
	allowedOrigins: ['http://localhost:8080', 'http://http://192.168.99.100:8080/']
}
